package u03

import org.junit.jupiter.api.Assertions._
import org.junit.jupiter.api.Test
import u02.Modules.Person.{Student, Teacher}

class TasksTest {

  import Lists._
  import Lists.List._
  import Streams._
  import Streams.Stream._
  import u02.Optionals.Option._

  @Test def testDrop(): Unit = {
    val lst = Cons (10, Cons(20, Cons(30, Nil())))
    assertEquals(List.drop(lst, 1), Cons(20, Cons(30, Nil())))
    assertEquals(List.drop(lst, 2), Cons(30, Nil()))
    assertEquals(List.drop(lst, 5), Nil())
  }

  @Test def testFlatmap(): Unit = {
    val lst = Cons (10, Cons(20, Cons(30, Nil())))
    assertEquals(List.flatMap(lst)(v => Cons(v+1, Nil())), Cons(11, Cons(21, Cons(31, Nil()))))
    assertEquals(List.flatMap(lst)(v => Cons(v+1, Cons(v+2, Nil()))), Cons(11, Cons(12, Cons(21, Cons(22, Cons(31, Cons(32, Nil())))))))
  }

  @Test def testMax() = {
    assertEquals(max(Cons(10, Cons(25, Cons(20, Nil())))), Some(25))
    assertEquals(max(Nil()), None())
  }

  @Test def testFilterTeachersAndCourses() = {
    assertEquals(Cons("ok", Cons("ok2", Nil())), filterTeachersAndCourses(Cons(Teacher("nome", "ok"), Cons(Student("nome", 1999), Cons(Teacher("nome2", "ok2"), Nil())))))
    assertEquals(Nil(), filterTeachersAndCourses(Cons(Student("nome", 1999), Nil())))
  }

  @Test def testFold() = {
    val lst = Cons(3, Cons(7, Cons(1, Cons(5, Nil()))))
    assertEquals(foldLeft(lst)(0)(_-_), -16)
    assertEquals(reverse(lst), Cons(5, Cons(1, Cons(7, Cons(3, Nil())))))
    assertEquals(foldRight(lst)(0)(_-_), -8)
  }

  @Test def testStreamDrop() = {
    val s = Stream.take(Stream.iterate(0)(_+1))(10)
    assertEquals(Stream.toList(Stream.drop(s)(6)), Cons(6, Cons(7, Cons(8, Cons(9, Nil())))))
  }

  @Test def testConstant() = {
    assertEquals(Stream.toList(Stream.take(constant("x"))(5)), Cons("x", Cons("x", Cons("x", Cons("x", Cons("x", Nil()))))))
  }

  /*
  @Test def testFib() = {
    val fibs = Stream.fib()
    assertEquals(Stream.toList(Stream.take(fibs)(8)), Cons(0, Cons(1, Cons(1, Cons(2, Cons(3, Cons(5, Cons(8, Cons(13, Nil())))))))))
  }
  */
}
