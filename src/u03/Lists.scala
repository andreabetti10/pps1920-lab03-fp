package u03

import u02.Modules.Person
import u02.Modules.Person.Teacher

object Lists {
  import u02.Optionals._
  import u02.Optionals.Option._
  // A generic linkedlist
  sealed trait List[E]

  // a companion object (i.e., module) for List
  object List {
    case class Cons[E](head: E, tail: List[E]) extends List[E]
    case class Nil[E]() extends List[E]

    def sum(l: List[Int]): Int = l match {
      case Cons(h, t) => h + sum(t)
      case _ => 0
    }

    def append[A](l1: List[A], l2: List[A]): List[A] = (l1, l2) match {
      case (Cons(h, t), l2) => Cons(h, append(t, l2))
      case _ => l2
    }

    def map[A,B](l: List[A])(mapper: A=>B): List[B] = l match {
      case Cons(h, t) => Cons(mapper(h), map(t)(mapper))
      case _ => Nil()
    }

    def filter[A](l1: List[A])(pred: A=>Boolean): List[A] = l1 match {
      case Cons(h,t) if (pred(h)) => Cons(h, filter(t)(pred))
      case Cons(_,t) => filter(t)(pred)
      case _ => Nil()
    }

    def drop[A](l: List[A], n: Int): List[A] = l match {
      case Cons(_, t) if n > 0 => drop(t, n - 1)
      case Cons(h, t) => Cons(h, t)
      case _ => Nil()
    }

    def flatMap[A,B](l: List[A])(f: A => List[B]): List[B] = l match {
      case Cons(h, t) => append(f(h), flatMap(t)(f))
      case _ => Nil()
    }

    def max(l: List[Int]): Option[Int] = {
      @annotation.tailrec
      def _max(l: List[Int], tempMax: Int) : Option[Int] = l match {
            case Cons(h, t) if (h > tempMax) => _max(t, tempMax = h)
            case Cons(_, t) => _max(t, tempMax)
            case _ if (tempMax > Int.MinValue) => Some(tempMax)
            case _ => None()
          }
      _max(l, Int.MinValue)
    }

    def filterTeachersAndCourses(l: List[Person]): List[String] = {
      flatMap(l)(e => e match {
        case Teacher(_, c) => Cons(c, Nil())
        case _ => Nil()
      })
    }

    @annotation.tailrec
    def foldLeft[A, B](l: List[A])(acc: B)(f: (B, A) => B) : B = l match {
        case Cons(h, t) => foldLeft(t)(f(acc, h))(f)
        case _ => acc
    }

    def reverse[A](l: List[A]) : List[A] = {
      @annotation.tailrec
      def _reverse(l: List[A], acc: List[A]): List[A] = l match {
        case Cons(h, t) => _reverse(t, Cons(h, acc))
        case _ => acc
      }
      _reverse(l, Nil())
    }

    def foldRight[A, B](l: List[A])(acc: B)(f: (A, B) => B) : B = {
      @annotation.tailrec
      def _foldRight(l: List[A])(acc: B)(f: (A, B) => B): B = {
        l match {
          case Cons(h, t) => _foldRight(t)(f(h, acc))(f)
          case _ => acc
        }
      }
      _foldRight(reverse(l))(acc)(f)
    }
  }
}

object ListsMain extends App {
  import Lists._
  val l = List.Cons(10, List.Cons(20, List.Cons(30, List.Nil())))
  println(List.sum(l)) // 60
  import List._
  import u03.Lists.List
  println(append(Cons(5, Nil()), l)) // 5,10,20,30
  println(filter[Int](l)(_ >=20)) // 20,30
}